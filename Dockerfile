FROM openjdk:8-jdk-alpine
COPY zipkin-server-2.17.1-exec.jar zipkin-server-2.17.1-exec.jar
EXPOSE 9411
ENTRYPOINT [ "sh", "-c", "java -jar /zipkin-server-2.17.1-exec.jar"]
